//	A compiler from a very simple Pascal-like structured language LL(k)
//	to 64-bit 80x86 Assembly langage
//	Copyright (C) 2019 Pierre Jourlin
//
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>
#include <map>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPES {UNSIGNED_INT, BOOLEAN, CHAR, DOUBLE};
enum SIGN  {POS, NEG};

TOKEN current;				// Current token

FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

map<string, int> DeclaredConstants;
map<string, enum TYPES> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}

bool IsConstant(const char *id){
	return DeclaredConstants.find(id)!=DeclaredConstants.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [VarDeclarationPart] StatementPart
// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
// VarDeclaration := id {"," id} ":" Type
// StatementPart := Statement {";" Statement} "."
// AssignementStatement := id "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | CharConst | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="	
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
enum TYPES Expression(void);			// Called by Term() and calls Term()
void Statement(void);

enum TYPES Identifier(void){
	enum TYPES type;
	if(!IsDeclared(lexer->YYText())) {
		cerr << "Variable '" << lexer->YYText() << "' non déclarée" <<endl;
		exit(-1);
	}
	type = DeclaredVariables[lexer->YYText()];
	if(IsConstant(lexer->YYText())) // On renvoie la valeur de la variable directement si elle est constante
		cout << "\tpush $" << DeclaredConstants[lexer->YYText()] <<endl;
	else
		cout<< "\tpush " << lexer->YYText() <<endl;
	current=(TOKEN) lexer->yylex();
	return type;
}

enum TYPES Number(void){
	double d;					// 64-bit float
	unsigned int *i;			// pointer to a 32 bit unsigned int 
	string	number=lexer->YYText();
	if(number.find(".")!=string::npos){ //Si c'est un float
		d=atof(lexer->YYText());
		i=(unsigned int *) &d; // i points to the const double
		// cout <<"\tpush $"<<*i<<"\t# Conversion of "<<d<<endl;
		// Is equivalent to : 
		cout <<"\tsubq $8,%rsp\t\t\t# allocate 8 bytes on stack's top"<<endl;
		cout <<"\tmovl	$"<<*(i)<<", (%rsp)\t# Conversion of "<<d<<" (32 bit high part)"<<endl;
		cout <<"\tmovl	$"<<*(i+1)<<", 4(%rsp)\t# Conversion of "<<d<<" (32 bit low part)"<<endl;
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else{ //Si c'est un entier
		cout << "\tpush $" << atoi(lexer->YYText()) <<endl;
		current=(TOKEN) lexer->yylex();
		return UNSIGNED_INT;
	}
}

enum TYPES CharConst(void){
	cout<<"\tmovq $0, %rax"<<endl;
	cout<<"\tmovb $"<<lexer->YYText()<<",%al"<<endl;
	cout<<"\tpush %rax\t# push a 64-bit version of "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return CHAR;
}


// Factor := Number | CharConst | "(" Expression ")"| ID
enum TYPES Factor(void){
	enum TYPES type;
	switch(current) {
		case RPARENT:
			current=(TOKEN) lexer->yylex();
			type = Expression();
			if(current!=LPARENT)
				Error("')' était attendu");		// ")" expected
			else
				current=(TOKEN) lexer->yylex();
			break;
		case NUMBER:
			type = Number();
			break;
		case ID:
			type = Identifier();
			break;
		case CHARCONST:
			type = CharConst();
			break;
		default:
			Error("'(' ou nombre ou lettre ou identifier attendu");
	}
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
enum TYPES Term(void){
	enum TYPES type1, type2;
	OPMUL mulop;
	type1 = Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		type2 = Factor();
		if(type1 != type2)
			Error("Types différents");
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
				break;
			case DIV:
				cout << "\tmovq $0, %rdx"<<endl;	// Higher part of numerator	 
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				break;
			case MOD:
				cout << "\tmovq $0, %rdx"<<endl;	// Higher part of numerator	 
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type1;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
enum TYPES SimpleExpression(void){
	OPADD adop;
	enum TYPES type1, type2;
	type1 = Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		type2 = Term();
		if(type1 != type2)
			Error("types différents");
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;			
			case SUB:	
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return type1;
}

enum TYPES Type(void){
	if(current != KEYWORD)
		Error("type attendu");
	if(strcmp(lexer->YYText(), "UNSIGNED_INT") == 0){
		current = (TOKEN)lexer->yylex();
		return UNSIGNED_INT;
	}
	else if(strcmp(lexer->YYText(), "BOOLEAN") == 0) {
		current = (TOKEN)lexer->yylex();
		return BOOLEAN;
	}
	else if(strcmp(lexer->YYText(), "CHAR") == 0) {
		current = (TOKEN)lexer->yylex();
		return CHAR;
	}
	else if(strcmp(lexer->YYText(), "DOUBLE") == 0) {
		current = (TOKEN)lexer->yylex();
		return DOUBLE;
	}
	else
		Error("Type inconnu");
}

// VarDeclaration := ID {"," ID} ":" Type
void VarDeclaration(void){
	set<string> idents;
	if(current!=ID)
		Error("Un identificateur était attendu");
	if(IsDeclared(lexer->YYText())) { // On vérifie que la variable n'a pas déjà été déclarée
		cerr << "Ligne n°" << lexer->lineno() << " ";
		if(IsConstant(lexer->YYText()))
			cerr << "Erreur : Constante '";
		else 
			cerr << "Erreur : Variable '";
		cerr << lexer->YYText() << "' déjà déclarée" <<endl;
		exit(-1);
	}
	idents.insert(lexer->YYText());
	current = (TOKEN)lexer->yylex();
	while(current==COMMA){
		current = (TOKEN)lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		if(IsDeclared(lexer->YYText())) { // On vérifie que la variable n'a pas déjà été déclarée
			cerr << "Ligne n°" << lexer->lineno() << " ";
			if(IsConstant(lexer->YYText()))
				cerr << "Erreur : Constante '";
			else 
				cerr << "Erreur : Variable '";
			cerr << lexer->YYText() << "' déjà déclarée" <<endl;
			exit(-1);
		}
		idents.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current != COLON)
		Error("caractère ':' attendu");
	current = (TOKEN)lexer->yylex();
	enum TYPES type = Type();
	for(set<string>::iterator it = idents.begin(); it != idents.end(); it++) {
		switch(type) {
			case BOOLEAN:
			case UNSIGNED_INT:
				cout<< *it << ":\t.quad 0" <<endl;
				break;
			case CHAR:
				cout << *it << ":\t.byte 0" <<endl;
				break;
			case DOUBLE:
				cout << *it << ":\t.double 0.0" <<endl;
				break;
			default:
				Error("Type inconnu");
		}
		DeclaredVariables[*it] = type;
	}
}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void){
	if(current == KEYWORD && strcmp(lexer->YYText(), "VAR") == 0)
		current = (TOKEN)lexer->yylex();
	else
		Error("\"VAR\" attendu");
	VarDeclaration();
	while(current == SEMICOLON){
		current = (TOKEN)lexer->yylex();
		VarDeclaration();
	}
	if(current != DOT)
		Error("'.' attendu");
	current = (TOKEN)lexer->yylex();
}
	

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="	
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
enum TYPES Expression(void){
	OPREL oprel;
	enum TYPES type1, type2;
	type1 = SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		type2 = SimpleExpression();
		if(type1 != type2)
			Error("Types différents");
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		return BOOLEAN;
	}
	return type1;
}

// AssignementStatement := Identifier ":=" Expression
string AssignementStatement(void){
	enum TYPES type1, type2;
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr<< "Ligne n°" << lexer->lineno() << " Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	if(IsConstant(lexer->YYText())){
		cerr<< "Ligne n°" << lexer->lineno() << " Erreur : Variable '" << lexer->YYText() << "' constante" <<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	type1 = DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	type2 = Expression();
	if(type1 != type2)
		Error("Types différents");
	cout << "\tpop "<<variable<<endl;
	return variable;
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.align 8"<<endl;
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// ConstantDefinitionPart := "CONST" ConstantDefinition {";" ConstantDefinition} "."
// ConstantDefinition := Identifier = Constant
// Constant := UnsignedInteger | Sign UnsignedInteger | Identifier
// Sign := "+" | "-"

int UnsignedInteger(SIGN signe) {
	int val = atoi(lexer->YYText());
	if(signe == NEG)
		cout << "-";
	cout<< val <<endl;
	current = (TOKEN)lexer->yylex();
	return val;
}

int ConstantIdentifier(SIGN signe) {
	if(!IsDeclared(lexer->YYText())){
		cerr<< "Ligne n°" << lexer->lineno() << " Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	int val = DeclaredConstants[lexer->YYText()];
	if(signe == NEG && val >= 0 || signe != NEG && val < 0)
		cout << "-";
	cout<< abs(val) <<endl;
	current = (TOKEN)lexer->yylex();
	return val;
}

// Sign := "+" | "-"
enum SIGN Sign(void) {
	enum SIGN Signe;
	if(strcmp(lexer->YYText(), "+")==0)
		Signe = POS;
	else if(strcmp(lexer->YYText(), "-")==0)
		Signe = NEG;
	else
		Error("Signe attendu");
	current = (TOKEN)lexer->yylex();
	return Signe;
}

// Constant := UnsignedInteger | Sign UnsignedInteger | Sign ConstantIdentifier | ConstantIdentifier
int Constant(void) {
	enum SIGN Signe;
	int val;
	if(current == NUMBER)
		val = UnsignedInteger(Signe);
	else if(strcmp(lexer->YYText(), "-")==0 || strcmp(lexer->YYText(), "+")==0) {
		Signe = Sign();
		if(current == NUMBER)
			val = UnsignedInteger(Signe);
		else if(current == ID)
			val = ConstantIdentifier(Signe);
	}
	else if(current == ID)
		val = ConstantIdentifier(Signe);
	else 
		Error("Nombre ou nombre signé ou identificateur attendu");
	if(Signe == NEG)
		return -val;
	else
		return val;
}

// ConstantDefinition := Identifier ":=" Constant
void ConstantDefinition(void) {
	string variable;
	int val;
	if(current != ID)
		Error("Identificateur attendu");
	if(IsDeclared(lexer->YYText())) { // On vérifie que la variable n'a pas déjà été déclarée
		cerr << "Ligne n°" << lexer->lineno() << " ";
		if(IsConstant(lexer->YYText()))
			cerr << "Erreur : Constante '";
		else 
			cerr << "Erreur : Variable '";
		cerr << lexer->YYText() << "' déjà déclarée" <<endl;
		exit(-1);
	}
	variable = lexer->YYText();
	DeclaredVariables[variable] = UNSIGNED_INT;
	cout << variable << ":\t.quad ";
	current = (TOKEN)lexer->yylex();
	if(current != ASSIGN)
		Error("Caractères ':=' attendu");
	current = (TOKEN)lexer->yylex();
	val = Constant();
	DeclaredConstants[variable] = val;
}
	

// ConstantDefinitionPart := "CONST" ConstantDefinition {";" ConstantDefinition} "."
void ConstDefinitionPart(void) {
	if(current == KEYWORD && strcmp(lexer->YYText(), "CONST") == 0)
		current = (TOKEN)lexer->yylex();
	else
		Error("Mot clé \"CONST\" attendu");
	ConstantDefinition();
	while(current == SEMICOLON) {
		current = (TOKEN)lexer->yylex();
		ConstantDefinition();
	}
	if(current != DOT)
		Error("Caractère '.' attendu");
	current = (TOKEN)lexer->yylex();
}
	
// Program := [ConstantDefinitionPart] [VarDeclarationPart] StatementPart
void Program(void){
	if(current == KEYWORD && strcmp(lexer->YYText(), "CONST") == 0)
		ConstDefinitionPart();
	if(current == KEYWORD && strcmp(lexer->YYText(), "VAR") == 0)
		VarDeclarationPart();
	StatementPart();	
}

// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement | DisplayStatement || CaseStatement
// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
// WhileStatement := "WHILE" Expression "DO" Statement
// ForStatement := "FOR" AssignementStatement "TO" Expression "DO" Statement
// BlockStatement := "BEGIN" Statement { ";" Statement } "END"
// CaseStatement := "CASE" Expression "OF" CaseListElement {";" CaseListElement} ["ELSE" Statement] "END"
// CaseListElement := CaseLabelList ":" Statement
// CaseLabelList := Factor {, Factor}
// RepeatStatement := "REPEAT" Statement "UNTIL" Expression

// RepeatStatement := "REPEAT" Statement "UNTIL" Expression
void RepeatStatement(void) {
	unsigned long tag=++TagNumber;
	if(current == KEYWORD && strcmp(lexer->YYText(), "REPEAT") == 0)
		current = (TOKEN)lexer->yylex();
	else 
		Error("Mot clé \"REPEAT\" attendu");
	cout<< "Repeat" << tag << " : " <<endl;
	Statement();
	if(current == KEYWORD && strcmp(lexer->YYText(), "UNTIL") == 0)
		current = (TOKEN)lexer->yylex();
	else 
		Error("Mot clé \"UNTIL\" attendu");
	if(Expression() != BOOLEAN)
		Error("Le type doit être BOOLEAN");
	cout<< "\tpop %rax" <<endl;
	cout<< "\tcmpq $0, %rax" <<endl; // Si l'expression est fausse (= 0)
	cout<< "\tje Repeat" << tag <<endl;
	cout<< "FinRepeat" << tag << " : " <<endl;
}

unsigned long TagCaseNumber = 0; // Sert à compter le nombre de cas dans un CASE

// CaseLabelList := Factor {, Factor}
enum TYPES CaseLabelList() {
	enum TYPES type1, type2;
	type1 = Factor();
	cout << "\tpop %rbx" <<endl;
	cout << "\tcmpq %rcx, %rbx" <<endl;
	cout << "\tje Case" << TagNumber << TagCaseNumber <<endl;
	while(current == COMMA) {
		current = (TOKEN)lexer->yylex();
		type2 = Factor();
		if(type1 != type2)
			Error("Types différents");
		cout << "\tpop %rbx" <<endl;
		cout << "\tcmpq %rcx, %rbx" <<endl;
		cout << "\tje Case" << TagNumber << TagCaseNumber <<endl;
	}
	cout << "\tjmp CaseSuite" << TagNumber << TagCaseNumber <<endl; 
	return type1;
}

// CaseListElement := CaseLabelList ":" Statement
enum TYPES CaseListElement() {
	unsigned long tagCase = ++TagCaseNumber;
	enum TYPES type = CaseLabelList();
	if(current != COLON)
		Error("Caractère ':' attendu");
	current = (TOKEN)lexer->yylex();
	cout<< "Case" << TagNumber << tagCase << " : " <<endl;
	Statement();
	cout<< "\tjmp FinCase" << TagNumber <<endl;
	cout<< "CaseSuite" << TagNumber << TagCaseNumber << " : " <<endl;
	return type;
}

// CaseStatement := "CASE" Expression "OF" CaseListElement {";" CaseListElement} ["ELSE" Statement] "END"
void CaseStatement(void) {
	enum TYPES type1, type2;
	TagCaseNumber = 0; // On remet à 0 le compteur de cas pour chaque nouveau CASE
	unsigned long tag=++TagNumber;
	if(current == KEYWORD && strcmp(lexer->YYText(), "CASE") == 0)
		current = (TOKEN)lexer->yylex();
	else 
		Error("Mot clé \"CASE\" attendu");
	type1 = Expression();
	cout << "\tpop %rcx" <<endl; // %rcx contient la valeur de l'expression jusqu'à la fin du case
	if(current == KEYWORD && strcmp(lexer->YYText(), "OF") == 0)
		current = (TOKEN)lexer->yylex();
	else 
		Error("Mot clé \"OF\" attendu");
	CaseListElement();
	while(current == SEMICOLON) {
		current = (TOKEN)lexer->yylex();
		type2 = CaseListElement();
		if(type1 != type2)
			Error("Type différents");
	}
	if(current == KEYWORD && strcmp(lexer->YYText(), "ELSE") == 0) {
		current = (TOKEN)lexer->yylex();
		Statement();
	}
	if(current == KEYWORD && strcmp(lexer->YYText(), "END") == 0)
		current = (TOKEN)lexer->yylex();
	else 
		Error("Mot clé \"END\" attendu");
	cout<< "FinCase" << tag << " : " <<endl;
}

// BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(void) {
	if(current == KEYWORD && strcmp(lexer->YYText(), "BEGIN") == 0)
		current = (TOKEN)lexer->yylex();
	else
		Error("Mot clé \"BEGIN\" attendu");
	Statement();
	while(current == SEMICOLON) {
		current = (TOKEN)lexer->yylex();
		Statement();
	}
	if(current == KEYWORD && strcmp(lexer->YYText(), "END") == 0)
		current = (TOKEN)lexer->yylex();
	else
		Error("Mot clé \"END\" attendu");
}

// ForStatement := "FOR" AssignementStatement ("TO"|"DOWNTO") Expression "DO" Statement
void ForStatement(void) {
	unsigned long tag=++TagNumber;
	enum TYPES type1, type2;
	char tmp1; // Stocke a ou b (pour above ou below)
	string tmp2; // Stokce add ou sub
	if(current == KEYWORD && strcmp(lexer->YYText(), "FOR") == 0)
		current = (TOKEN)lexer->yylex();
	else
		Error("Mot clé \"FOR\" attendu");
	string var = AssignementStatement();
	type1 = DeclaredVariables[var];
	if(current == KEYWORD && strcmp(lexer->YYText(), "TO") == 0) {
		current = (TOKEN)lexer->yylex();
		tmp1 = 'a';
		tmp2 = "add";
	}
	else if(current == KEYWORD && strcmp(lexer->YYText(), "DOWNTO") == 0) {
		current = (TOKEN)lexer->yylex();
		tmp1 = 'b';
		tmp2 = "sub";
	}
	else
		Error("Mot clé \"TO\" attendu");
	type2 = Expression();
	if(type1 != type2)
		Error("Types différents");
	if(current == KEYWORD && strcmp(lexer->YYText(), "DO") == 0)
		current = (TOKEN)lexer->yylex();
	else
		Error("Mot clé \"DO\" attendu");
	cout<< "\tpop %rcx" <<endl;
	cout<< "\tcmpq	%rcx, " << var <<endl; // On compare une fois au début du for (initialisation)
	cout<< "\tj" << tmp1 << " FinFor" << tag <<endl; 
	cout<< "For" << tag << " : " <<endl;
	Statement();
	cout<< "\tcmpq %rcx, " << var <<endl; // On compare une deuxième fois car sinon ça ne marche pas pour DOWNTO 0
	cout<< "\tj" << tmp1 << "e FinFor" << tag <<endl;
	cout<< "\tpush " << var <<endl;
	cout<< "\tpush $1" <<endl;
	cout<< "\tpop %rbx" <<endl;
	cout<< "\tpop %rax" <<endl;
	cout<< "\t" << tmp2 << "q %rbx, %rax" <<endl;
	cout<< "\tpush %rax" <<endl;
	cout<< "\tpop " << var <<endl;
	cout<< "\tjmp For" << tag <<endl;
	cout<< "FinFor" << tag << " : " <<endl;
	
}
	

// WhileStatement := "WHILE" Expression "DO" Statement
void WhileStatement(void) {
	unsigned long tag=++TagNumber;
	if(current == KEYWORD && strcmp(lexer->YYText(), "WHILE") == 0)
		current = (TOKEN)lexer->yylex();
	else
		Error("Mot clé \"WHILE\" attendu");
	cout<< "While" << tag << " : " <<endl;
	if(Expression() != BOOLEAN)
		Error("Le type doit être BOOLEAN");
	if(current == KEYWORD && strcmp(lexer->YYText(), "DO") == 0)
		current = (TOKEN)lexer->yylex();
	else
		Error("Mot clé \"DO\" attendu");
	cout<< "\tpop %rax" <<endl;
	cout<< "\tcmpq $0, %rax" <<endl; // Si l'expression est fausse (= 0)
	cout<< "\tje FinWhile" << tag <<endl;
	Statement();
	cout<< "\tjmp While" << tag <<endl;
	cout<< "FinWhile" << tag << " : " <<endl;
	
}

// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void) {
	unsigned long tag=++TagNumber;
	if(current == KEYWORD && strcmp(lexer->YYText(), "IF") == 0)
		current = (TOKEN)lexer->yylex();
	else
		Error("Mot clé \"IF\" attendu");
	if(Expression() != BOOLEAN)
		Error("Le type doit être BOOLEAN");
	if(current == KEYWORD && strcmp(lexer->YYText(), "THEN") == 0)
		current = (TOKEN)lexer->yylex();
	else
		Error("Mot clé \"THEN\" attendu");
	cout<< "\tpop %rax" <<endl;
	cout<< "\tcmpq $0, %rax" <<endl;
	cout<< "\tje Else" << tag <<endl;
	Statement();
	cout<< "\tjmp FinIf" << tag <<endl;
	cout<< "Else" << tag << " : " <<endl;
	if(current == KEYWORD && strcmp(lexer->YYText(), "ELSE") == 0)
	{
		current = (TOKEN)lexer->yylex();
		Statement();
	}
	cout<< "FinIf" << tag << " : " <<endl;
}

// DisplayStatement := "DISPLAY" Expression
void DisplayStatement(void){
	enum TYPES type;
	unsigned long long tag=++TagNumber;
	current=(TOKEN) lexer->yylex();
	type=Expression();
	switch(type){
	case UNSIGNED_INT:
		cout << "\tpop %rsi\t# The value to be displayed"<<endl;
		cout << "\tmovq $FormatString1, %rdi\t# \"%llu\\n\""<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	printf@PLT"<<endl;
		break;
	case BOOLEAN:
		cout << "\tpop %rdx\t# Zero : False, non-zero : true"<<endl;
		cout << "\tcmpq $0, %rdx"<<endl;
		cout << "\tje False"<<tag<<endl;
		cout << "\tmovq $TrueString, %rdi\t# \"TRUE\\n\""<<endl;
		cout << "\tjmp Next"<<tag<<endl;
		cout << "False"<<tag<<":"<<endl;
		cout << "\tmovq $FalseString, %rdi\t# \"FALSE\\n\""<<endl;
		cout << "Next"<<tag<<":"<<endl;
		cout << "\tcall	puts@PLT"<<endl;
		break;
	case DOUBLE:
			cout << "\tmovsd	(%rsp), %xmm0\t\t# &stack top -> %xmm0"<<endl;
			cout << "\tsubq	$16, %rsp\t\t# allocation for 3 additional doubles"<<endl;
			cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
			cout << "\tmovq $FormatString2, %rdi\t# \"%lf\\n\""<<endl;
			cout << "\tmovq	$1, %rax"<<endl;
			cout << "\tcall	printf"<<endl;
			cout << "nop"<<endl;
			cout << "\taddq $24, %rsp\t\t\t# pop nothing"<<endl;
			break;
	case CHAR:
			cout<<"\tpop %rsi\t\t\t# get character in the 8 lowest bits of %si"<<endl;
			cout << "\tmovq $FormatString3, %rdi\t# \"%c\\n\""<<endl;
			cout << "\tmovl	$0, %eax"<<endl;
			cout << "\tcall	printf@PLT"<<endl;
			break;

	default:
			Error("DISPLAY ne fonctionne pas pour ce type de donnée.");
		}

}


// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement
void Statement(void) {
	if(current == ID)
		AssignementStatement();
	else if(current == KEYWORD && strcmp(lexer->YYText(), "IF") == 0)
		IfStatement();
	else if(current == KEYWORD && strcmp(lexer->YYText(), "WHILE") == 0)
		WhileStatement();
	else if(current == KEYWORD && strcmp(lexer->YYText(), "FOR") == 0)
		ForStatement();
	else if(current == KEYWORD && strcmp(lexer->YYText(), "DISPLAY") == 0)
		DisplayStatement();
	else if(current == KEYWORD && strcmp(lexer->YYText(), "BEGIN") == 0)
		BlockStatement();
	else if (current == KEYWORD && strcmp(lexer->YYText(), "CASE") == 0)
		CaseStatement();
	else if (current == KEYWORD && strcmp(lexer->YYText(), "REPEAT") == 0)
		RepeatStatement();
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << ".data"<<endl;
	cout << "FormatString1:\t.string \"%llu\"\t# used by printf to display 64-bit unsigned integers"<<endl; 
	cout << "FormatString2:\t.string \"%lf\"\t# used by printf to display 64-bit floating point numbers"<<endl; 
	cout << "FormatString3:\t.string \"%c\"\t# used by printf to display a 8-bit single character"<<endl; 
	cout << "TrueString:\t.string \"TRUE\"\t# used by printf to display the boolean value TRUE"<<endl; 
	cout << "FalseString:\t.string \"FALSE\"\t# used by printf to display the boolean value FALSE"<<endl; 

	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}
}